﻿using System;
using Newtonsoft.Json;

namespace electra_signature.Models
{
    public class GetProductBalanceRequest : PayloadRequestModel
    {
        [JsonProperty(PropertyName = "product_id", Required = Required.Always)]
        public String ProductId { get; set; }
    }
}

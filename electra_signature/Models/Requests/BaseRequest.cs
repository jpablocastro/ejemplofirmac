﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace electra_signature.Models
{
    public class BaseRequest
    {
        [JsonProperty(PropertyName = "timestamp", Required = Required.Default)]
        public long Timestamp { get; set; }

        [JsonProperty(PropertyName = "sort", Required = Required.Default)]
        public SortModel Sort { get; set; }

        [JsonProperty(PropertyName = "search", Required = Required.Default)]
        public Dictionary<String, SearchModel> Search { get; set; }

        [JsonProperty(PropertyName = "payload", Required = Required.Default)]
        public PayloadRequestModel Payload { get; set; }

        [JsonProperty(PropertyName = "paginate", Required = Required.Default)]
        public PaginateModel Paginate { get; set; }

        [JsonProperty(PropertyName = "external_reference", Required = Required.Always)]
        public String ExternalReference { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace electra_signature.Models
{

    public class BaseResponse
    {
        [JsonProperty(PropertyName = "timestamp", Required = Required.Default)]
        public long Timestamp { get; set; }

        [JsonProperty(PropertyName = "sort", Required = Required.Default)]
        public SortModel Sort { get; set; }

        [JsonProperty(PropertyName = "search", Required = Required.Default)]
        public Dictionary<String, SearchModel> Search { get; set; }


        //public PayloadResponseModel Payload { get; set; }

        [JsonProperty(PropertyName = "paginate", Required = Required.Default)]
        public PaginateModel Paginate { get; set; }

        [JsonProperty(PropertyName = "external_reference", Required = Required.Default)]
        public String ExternalReference { get; set; }

        [JsonProperty(PropertyName = "transaction_id", Required = Required.Always)]
        public String TransactionID { get; set; }

        [JsonProperty(PropertyName = "status", Required = Required.Always)]
        public String Status { get; set; }
    }
}


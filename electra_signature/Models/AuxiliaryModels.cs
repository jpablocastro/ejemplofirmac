﻿using System;
using Newtonsoft.Json;

namespace electra_signature.Models
{
    public static class SearchTypes
    {
        public const String IsEqual = "eq";
    }

    public static class OrderOptions
    {
        public const String Ascending = "asc";
        public const String Descending = "desc";
    }

    public class SearchModel
    {
        public String SearchType { get; set; }
        public String SearchTerm { get; set; }
    }

    public class PaginateModel
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
    }

    public class SortModel
    {
        public String Field { get; set; }
        public String Order { get; set; }
    }
}

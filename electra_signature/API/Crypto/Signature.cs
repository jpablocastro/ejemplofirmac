﻿using System;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;

namespace electra_signature.API.Crypto
{
    public static class Signature
    {
        public const String PrivateKeyPem = @"-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArvYLy6rkIzm9kosH7Oap0BOntzFg33jxCIfBKUKaB1XtGXJK
UpXy1EMv7lJqYS/ETUsUY0mVs9N+rot/vBBv5NeOAG4dPKV1y57rKwUoUms9wGFk
1wNugwnoaAvaD7W4S6BMVpcjcfyd2rHj+MOl8Acy00HUYkWgPs2XKyqcndVwV+JN
zKAZNrdmAeVCqAvZQLuk4uugry6+GOwwppJhbg3G4iSML7T8kMx9aiePYY7EhTKY
AfR9w9XsnY3nzE2BJRA0F+wCC9X2nvsFAcJxOxYS9DUnj2fwqUz8Tj2G8/OZW1N0
PtXDx+CjZQAFU0rIT/F1nGzFbMH39n9can4wbQIDAQABAoIBAFrUY0VFn5Bl/nOg
d+nVydy7B3vER71cRQ2fpHjuGZarVh66xbjgHaNyieSYpp9zs95IlvoAjpY/uumU
hCi3A8/y08aHY+hDIccm5oF7+PXotLM2HKq/HlUrBJtXY8p6FYY1XswpRH59ndmW
NnrD31af9hPUfhTSfqF8C/md8pbBBvso3gsMu0z17WQ8RtwspaLl82VBN9yC+W/s
RU9AQyyAJhl00PUegErvWWziMjfhHgj078svjf0572mDat0ZzVGVon2AJUcQcIKS
8CT8KpdVn48QEwgv2KaIE82rPEm7trXt2WHC90L54c2HreoP93IkylZLtHA/aBs4
a1FJImECgYEA1/XB58mvjQQVH91i09t9eYuRw6YqyAoNwOmTGICIk4TeKl/LnroM
yMOSEh/BmPjeUfBRbjX5AMDD4dPz2c1NdOaZi3ezLpKP7USuqtmsmyFKLD52VvIY
2ba3y5Oa6VRVPAFSq0UKV4av+gR5PewTkvsnckmRTY6cX4hcn92FROUCgYEAz2ZV
c3Fp1krHGGqEro6Hg1qS6N2NDEdqJ+iowpXpwzh5kDuBqy2nQjrj7oz4BDFj9qfJ
0x+LfMuhwHtAlnAw6ka7q9SIpzyVo7i7jmYF7t/lLRwfwe688ip13yEy7rp5EDE9
VqWI3rzWEBxtgFpbhFIZ5SSY8pzE6apk3wtczOkCgYEAx49Bb37fyd9L5fYdrMfW
WsquF60lO1SPAQTV+H5NtoPBCEcdUb96sMqH4FeX/lN6nR0FImKdhiSa3cOOQmWE
fdHZcRdbbF3iwc4F2brr1S4d7GSL1ixA6m39yRDMxXA4kwpl6f4SYG8FjnMz1hSg
weFzwhhZPOHbfivz64Y38EECgYEApMqR8QVXa/7EjUDtpej4AcWw3zhSf0QrNASo
Otx0/jncVV8C2+mNcZFrVDtMQcCgxfZx5b+1zHXcUx1bp20DYNLNAW4BGqiH2avJ
0w57fl8GML6c+THhjaVxPNabKpad5ODK9RLYWuq+R9TeqNKiztORYcGfiX5I88UB
GzxjAykCgYAtJAMobBAOZXitLFJ+Yih5qQOYEI9HfRa7FVJGzcyHaQpunHdENhuN
ZrRmKITkRfiorYk/SDQO7FFtVuBAipusySNvWIdBC5MZKBEo7wKJwCCa8U7idqVg
7GWT+8SJK4dELyxzOqrdbGghJMqUr1IWdMRof1SyTxgtPMLecW6Nlw==
-----END RSA PRIVATE KEY-----";

        public static String Sign(long TimeStamp, String Payload)
        {
            byte[] payloadSHA256 = ToSHA256(TimeStamp.ToString() + "|" + Payload);
            var encryptEngine = new Pkcs1Encoding(new RsaEngine());
            using (var txtreader = new StringReader(PrivateKeyPem))
            {
                var keyPair = (AsymmetricCipherKeyPair)new PemReader(txtreader).ReadObject();
                encryptEngine.Init(true, keyPair.Private);
            }
            string encrypted = Convert.ToBase64String(encryptEngine.ProcessBlock(payloadSHA256, 0, payloadSHA256.Length));
            String ReturnValue = encrypted.Replace('+', '-').Replace('/', '_');
            return ReturnValue;
        }

        private static byte[] ToSHA256(string input)
        {
            byte[] encData = System.Text.Encoding.UTF8.GetBytes(input);
            Org.BouncyCastle.Crypto.Digests.Sha256Digest myHash = new Org.BouncyCastle.Crypto.Digests.Sha256Digest();
            myHash.BlockUpdate(encData, 0, encData.Length);
            byte[] compArr = new byte[myHash.GetDigestSize()];
            myHash.DoFinal(compArr, 0);
            return compArr;
        }

    }
}
